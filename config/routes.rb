Rails.application.routes.draw do
  
  root 'pages#home'
  
  get 'streams/new'
  get 'streams/edit'
  get 'streams/show'
  get 'streams/index'

  get 'activities/new'
  get '/new-activity' => 'activities#new'
  get 'activities/show'
  get 'activities/index'
  get 'activities/edit'

  get 'sessions/new'
  get '/login' => 'sessions#new'

  get 'users/new'
  get '/signup' => 'users#new'
  get 'users/show'
  get 'users/edit'
  get 'users/index'

  resources :users
  resources :activities
  resources :streams

  get 'pages/home'
  get 'pages/about'
  get 'pages/help'
  get 'pages/pro'

  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

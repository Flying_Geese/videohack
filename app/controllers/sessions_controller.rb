class SessionsController < ApplicationController
  def new
    redirect_to root_url if logged_in?
  end

  def create
  	user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      log_in user
      redirect_to user
else
  	flash.now[:danger] = 'Invalid email or password' # create error message
  	render 'new'
  end
end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
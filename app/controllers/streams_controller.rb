class StreamsController < ApplicationController
  require "opentok"
  include ActionController::Live

  def new
  	if !logged_in?
  		redirect_to login_path
    else
      @stream = Stream.new
  	end
  end

  def create
    @stream = current_user.streams.build(stream_params)
    @stream.activity_id = params[:activity_id]
    if @stream.save
      @stream.save
      redirect_to @stream
    else
      puts "#{@stream.errors.full_messages}"
      render 'new'
    end
  end

  def edit
  end

  def show
    @stream = Stream.find(params[:id])
    start_stream
  end

  def index
  end

   def start_stream
        # Replace with your OpenTok API key:
        api_key = "45793422"
        # Replace with your OpenTok API secret:
        api_secret = "21bdd1ff828e42ebb1b21755d39b1d4307be5a88"

        opentok = OpenTok::OpenTok.new api_key, api_secret 
        session = opentok.create_session :media_mode => :routed
        session_id = session.session_id
        token = session.generate_token
        # archive = opentok.archives.create session

    end

    private

    def stream_params
      params.require(:stream).permit(:title, :description)
      
    end

end

class ActivitiesController < ApplicationController
  def new
  	@activity = Activity.new
  end

  def create
  	@activity = Activity.new(activity_params)
  	if @activity.save
  		redirect_to @activity
  	else
  		render 'new'
  	end
  end

  def show
  end

  def index
    @activities = Activity.all.order('name ASC')
  end

  def edit
  end

  private

  def activity_params
  	params.require(:activity).permit(:name, :description, :image)
  end
end

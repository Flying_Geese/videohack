class User < ApplicationRecord
	attr_accessor :remember_token
	before_save { self.email = email.downcase }
  before_save { self.username = username.downcase }
  validates :first_name, :last_name,  presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 100 },
                    format: { with: VALID_EMAIL_REGEX }, uniqueness: true
  validates :username, presence: true, length: { minimum: 3, maximum: 32 }                  
	has_secure_password
	validates :password, presence: true, length: { minimum: 8 }, allow_nil: true
	has_attached_file :image,
                    styles: { normal: '180x180#', thumb: '32x32#', medium: '64x64#' }
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
	has_many :streams

	def full_name
    first_name.capitalize + " " + last_name.capitalize
  end

  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

 def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end
end

 

def User.new_token
    SecureRandom.urlsafe_base64
  end

  def forget
    update_attribute(:remember_digest, nil)
  end


private

def downcase_email
	self.email = email.downcase
end



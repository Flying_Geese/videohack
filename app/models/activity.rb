class Activity < ApplicationRecord
	has_many :streams
	validates :name, :description, presence: true
	has_attached_file :image,
                    styles: { normal: '364x220#', thumb: '32x32#', medium: '64x64#' }
                    validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
end
